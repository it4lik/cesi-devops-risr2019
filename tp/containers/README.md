# Containers - First steps

On va aborder dans ce TP les bases de l'utilisation de la conteneurisation.

Afin d'évoluer dans un environnement standard (rendant notamment plus simple le débugging), je vous conseille d'utiliser :
* une VM CentOS7 pour héberger Docker (utilisez Vagrant !)
* une VM avec un autre Linux commun (Debian, Ubuntu, etc.)

Vous pouvez aussi utiliser votre machine hôte, si vous maîtrisez l'environnement.

> Les instructions et commandes seront données pour CentOS7.

<!-- vim-markdown-toc GitLab -->

* [I. Mise en place de l'environnement](#i-mise-en-place-de-lenvironnement)
* [II. Manipulation de conteneurs](#ii-manipulation-de-conteneurs)
    * [Commandes de base](#commandes-de-base)
    * [Lancement d'un premier conteneur](#lancement-dun-premier-conteneur)
* [III. Manipulation d'images](#iii-manipulation-dimages)
    * [Gestion d'images](#gestion-dimages)
    * [Création d'images](#création-dimages)
    * [Création d'un Dockerfile](#création-dun-dockerfile)
* [IV. `docker-compose`](#iv-docker-compose)
    * [Fonctionnement](#fonctionnement)
    * [Création d'un `docker-compose.yml`](#création-dun-docker-composeyml)

<!-- vim-markdown-toc -->

# I. Mise en place de l'environnement

Dans le cas de l'utilisation d'une machine GNU/Linux, il est préférable de mettre à jour le système.
```bash
$ sudo yum update -y
```

Installer Docker en suivant [la documentation officielle](https://docs.docker.com/install/).

---

Afin d'utiliser les commandes `docker` sans avoir besoin des droits `root`, il faut que l'utilisateur que vous utilisez soit membre du groupe `docker`.
```bash
# Ajout de l'utilisateur courant au groupe 'docker'
$ sudo usermod -aG docker $(whoami)

# Il sera nécessaire de vous déconnecter puis reconnecter (juste déconnexion, no need reboot).
```

Démarrer le service `docker` :
```bash
$ sudo systemctl enable --now docker
$ systemctl status docker
```

Tester le bon fonctionnement de `docker` :
```bash
$ docker info

$ docker run hello-world
```

# II. Manipulation de conteneurs

## Commandes de base

Une fois `docker` fonctionnel, vous pouvez utilisez l'ensemble des commandes Docker. 

Commandes de base :
```bash
# Liste les conteneurs actifs
$ docker ps

# Liste tous les conteneurs (y compris inactifs)
$ docker ps -a

# Lister les images disponibles
$ docker images

# Lancer un conteneur
$ docker run <IMAGE>

# Afficher les logs d'un conteneur
$ docker logs <CONTAINER>

# Supprimer un conteneur
$ docker rm <CONTAINER>
```

Pour pouvoir lancer des conteneurs, il faut récupérer une image à lancer.

**Il existe un répertoire d'image publique**, mis à disposition par la société Docker : **le [Docker Hub](https://hub.docker.com/).**

## Lancement d'un premier conteneur

```bash
# Lancement du conteneur
$ docker run debian
```

L'image `debian` est automatiquement récupérée sur le Hub.

```bash
$ docker ps -a
```

Le conteneur est visible, mais il est éteint. En effet, un conteneur simple comme `debian` n'est pas configuré pour lancer une commande au démarrage.

Or, un conteneur ne peut s'exécuter que si on lui dit de lancer un processus.

Go :
```bash
# Lancement d'un conteneur qui dort pendant 9999 secondes
$ docker run debian sleep 9999

# Voir le conteneur en activité
$ docker ps

# Récupérer un shell dans le conteneur
$ docker exec -it <NAME_OR_ID> sh
```

Une fois que avez fini de jouer, n'oubliez pas de détruire le conteneur :)

---

Vous savez maintenant : 
* lancer un conteneur
* consulter sa sortie, les logs
* récupérer un shell dans le conteneur
* détruire un conteneur

# III. Manipulation d'images

## Gestion d'images

```bash
# Lister les images disponibles
$ docker images

# Récupérer une image depuis le Hub
$ docker pull <IMAGE>
```

Les images possèdent, en plus de leur nom, un *tag*. Ce *tag* fait référence à la version du conteneur. Quand aucun tag n'est précisé, le *tag* `latest` est assumé par défaut.

Une même image peut porter des tags différents.
```bash
# Récupéraion de la dernière image Debian
$ docker pull debian
$ docker pull debian:latest
$ docker pull debian:buster

# Récupération de la version "slim"
$ docker pull debian:buster-slim
```

**Les images sont composées de couches ou *layers*. Si vous possédez déjà un *layer* donné, vous ne le stockerez pas une deuxième fois.**

## Création d'images

Afin de créer une image, il est nécessaire de rédiger un fichier `Dockerfile`. Le `Dockerfile` est un simple fichier texte qui décrit les opérations à réaliser pour construire l'image.

Un `Dockerfile` simple peut ressembler à (n'hésitez pas à virer les commentaires pour voir plus clair:

```Dockerfile
# Un Dockerfile se base toujours sur une image déjà existante
FROM debian

# Ceci est à but informatif : le conteneur écoutera sur le port 8888
EXPOSE 8888 

# Exécution de commandes
RUN apt-get update -y \
    && apt-get install -y python3

# Définition d'un répertoire de travail (toutes les commande suivantes seront exécutées depuis ce répertoire)
WORKDIR /opt/                   

# Création d'un fichier index.html de test
RUN echo test > "index.html"      

# Commande lancée au démarrage du conteneur
ENTRYPOINT [ "/usr/bin/python3", "-m", "http.server", "8888" ]
```

Une fois le `Dockerfile` créé, on peut utiliser la commande `docker build` pour construire notre image.

```bash
$ ls
Dockerfile

# Construction de l'image
# Le . est important : il indique l'emplacement du Dockerfile ( . est une référence vers le dossier actuel)
$ docker build . -t test-web 

# Vérification
$ docker images

# Test
# le -d permet de lancer notre conteneur en tâche de fond
# le -p permet de partager un port du conteneur vers l'hôte
$ docker run -p 8888:8888 -d test-web

# Test du bon fonctionnement du conteneur
$ curl localhost:8888

# Visualisation des logs du conteneur
$ docker logs <CONTAINER_NAME_OR_ID>
$ docker logs <CONTAINER_NAME_OR_ID> -f # ajout d'une option pour suivre les logs en direct
```

Il est possible de faire plein de choses dans le Dockerfile. [Read the docs !](https://docs.docker.com/engine/reference/builder/).

---

Petit mémo des instruction possibles dans un Dockerfile :
* `FROM` : pour spécifier une image de base
* `RUN` : pour exécuter des commandes dans le conteneur
* `COPY` : pour copier des fichier depuis l'hôte dans l'image
* `WORKDIR` : pour spécifier un répertoire de travail
  * tout le reste du Dockerfile sera exécuté depuis ce répertoire
* `ENTRYPOINT` : détermine la commande à lancer lorsque le conteneur se lancera

## Création d'un Dockerfile

Création d'une image par vous-même :

* créer votre propre image NGINX
  * doit se baser sur une image existante Debian
    * mot-clé `FROM`
    * vous devez préciser un tag explicite
  * installation du paquet correspondant à NGINX
    * mot-clé `RUN`
  * doit lancer un serveur NGINX au démarrage
    * mot-clé `ENTRYPOINT`

**Vérifier que ça fonctionne en lançant le conteneur :**
* lancer le conteneur avec `docker run`
* utiliser l'option `-p` pour partager un port et accéder au serveur Web
* utiliser l'option `-v` pour choisir un `index.html` différent que vous avez créé sur votre hôte
* utiliser un navigateur pour visiter le site web

# IV. `docker-compose`

`docker-compose` est un outil permettant de lancer plusieurs conteneurs de façon simultanée. Plutôt que de fair eplusieurs `docker run` complexes, on lance tout dans une seule commande.

Installez `docker-compose` en suivant la doc officielle Docker.

## Fonctionnement

Pour fonctionner, il faut donner à `docker-compose` un fichier `docker-compose.yml`. Un fichier simpliste pourrait ressembler à :
```yml
version: "3.7"

networks:
  web:

services:
  web:
    image: nginx
    ports:
      - "8888:80"
    networks:
      web:
        aliases:
          - "web"
          - "web.test"
  client:
    image: debian
    networks:
      web:
        aliases:
          - "client"
          - "client.test"
```

Puis :
```bash
$ ls
docker-compose.yml

# Lancer les deux conteneurs
$ docker-compose up

# Lancer les deux conteneurs en arrière-plan
$ docker-compose up -d
```

Pour tester :

* vérifier que vous pouvez, **depuis votre hôte** joindre le conteneur web sur le port 8888 **de votre hôte**
* afficher les conteneurs actifs
* récupérez un shell dans le conteneur client
  * testez le `ping` vers "web" et "web.test"
  * testez avec un `curl` pour vérifier que vous pouvez joindre le serveur web
    * `curl <IP:PORT>` pour faire une requête HTTP avec `curl`
    

## Création d'un `docker-compose.yml`

Afin de vous mettre dans une situation un peu réelle :

* j'ai codé une petite application Web en Python
* elle a besoin d'une base de données Redis pour fonctionner
  * l'application Python doit pouvoir joindre la base de données Redis sur le nom "db"
* l'application a quelques dépendances qu'il faudra installer
  * l'application utilise `python3`
  * les librairies nécessaires sont dans le fichier `requirements`
    * vous pouvez faire la commande `pip3 install -r requirements` pour installer les librairies
* pour lancer l'app : `python3 app.py`
  * l'application écoute sur le port 8888/tcp

Vous trouverez [le code de l'application dans le dossier dédié](./python-app).

Le but :

* créer un `Dockerfile` qui contient l'application et ses dépendances
  * pour le `FROM` il existe des images `python` sur le Docker Hub
  * vous allez utiliser `RUN` pour mettre à jour l'image si besoin est
  * la clause `COPY` sera nécessaire pour mettre l'application fournie dans le conteneur
  * il faudra utiliser `RUN` pour lancer la commande `pip3 install -r requirements`
  * il faudra utiliser `ENTRYPOINT` pour lancer le serveur web avec la commande `python3 app.py`
  * **testez !**
* créer un `docker-compose.yml` :
  * il lance l'application Python
  * il lance une base de donnée Redis, disponible sur le nom "db"
  * **testez !**

> Vous pouvez utiliser `ADD` dans le Dockerfile pour ajouter des fichiers dans l'image depuis votre hôte. Référez-vous à la doc !
